## KI2015 Assignment 5

### Aufgabe 1

    p(X,f(Y),Z)
    p(T,T,g(cat))
    p(f(dog),S,g(W))

**1a**: Disagreement Set {X, T, f(dog)}

**1b**: Angewendet wird hier der unification algorithm von der Webseite
    von George Sutcliffe. Bei dem man immer abwechselnd Disagreement Sets (DS)
    erstellt und dann einzelne Variablen substituiert.
    
    DS {X, T, f(dog)} -> X/f(dog)
    DS {f(dog), T} -> T/f(dog)
    DS {f(Y), f(dog), S} -> S/f(dog)
    DS {Y, dog} -> Y/dog
    DS {Z, g(cat), g(W)} -> Z/g(cat)
    DS {cat, W} -> W/cat

Daraus folgt folgender Most General Unifier:

```plaintext    
{X/f(dog), T/f(dog), S/f(dog), Y/dog, Z/g(cat), W/cat}
```

### Aufgabe 2

    S = { p(X) | q(X),          (1)
          ~p(Z) | q(Z),         (2)
          p(Y) | ~q(Y),         (3)
          ~p(U) | ~q(U) }       (4)

**2a**: Wenn man Binary Resolution ohne Factoring verwendet, passiert folgendes:

    (1+2)   { q(X) |  q(X)}
    (2+3)   {~q(Y) | ~q(Y)}

Da man ja immer nur zwei Literale miteinander "auflösen" kann und nicht mehrere
auf einmal, wird man immer wieder Klauseln mit 2 Literalen erstellen beim
resolvieren mit Binary Resolution ohne Factoring. Mit Factoring könnte man die
beiden Gleichungen vereinfachen und würde zu einem Ergebnis kommen.

**2b**: Mit Full Resolution würde es so aussehen:

    (1+2=5)   { q(X)}
    (2+3=6)   {~q(Y)}
    (5+6)     {}

### Aufgabe 3

    (A1) ∀X (politiker(X) ==> (∀Y (knausrig(Y ) ==> ~mag(X,Y ))))
    (A2) ∀X (politiker(X) ==> (∃Y (firma(Y) & mag(X,Y))))
    (A3) ∃X politiker(X)
    (B) ∃X (firma(X) & ~knausrig(X))

**3a**:

(A1) in Klauselnormalform bringen:

    ∀X (politiker(X) ==> (∀Y (~knausrig(Y) | ~mag(X,Y))))
    ∀X ∀Y (~politiker(X) | ( (~knausrig(Y) | ~mag(X,Y))))
    (~politiker(X) | ~knausrig(Y) | ~mag(X,Y))

(A2) in Klauselnormalform bringen:

    ∀X (~politiker(X) | (∃Y (firma(Y) & mag(X,Y))))
    ∀X ∃Y (~politiker(X) | (firma(Y) & mag(X,Y)))
    ∃Y (~politiker(X) | (firma(Y) & mag(X,Y))) y={X}
    (~politiker(X) | (firma(sk1(X)) & mag(X,sk1(X))))
    (~politiker(X) | firma(sk1(X)) & (~politiker(X) | mag(X,sk1(X))))

(A3) in Klauselnormalform bringen:

    ∃X politiker(X)
    politiker(sk2)

~(B) in Klauselnormalform bringen:

    ~(∃X (firma(X) & ~knausrig(X)))
    ∀X ~(firma(X) & ~knausrig(X))
    (~firma(X) | knausrig(X))

**3b**: Nun muss man (A1) & (A2) & (A3) & ~(B) resolvieren,
    bis man die leere Menge abgeleitet hat:

    S = { ~politiker(X) | ~knausrig(Y) | ~mag(X,Y),   (1)
          ~politiker(Z) | firma(sk1(Z)),              (2)
          ~politiker(U) | mag(U,sk1(U)),              (3)
          politiker(sk2),                             (4)
          ~firma(V) | knausrig(V) }                   (5)

    (1+5=6) θ={V/Y}
            {~politiker(X) | ~mag(X,Y) | ~firma(Y)}

    (2+6=7) θ={X/Z, Y/sk1(Z)}
            {~politiker(Z), ~mag(Z,sk1(Z)}

    (7+3=8) θ={Z/U}
            {~politiker(U)}

    (8+4=9) θ={U/sk2}
            {}

### Aufgabe 4

The occurs check can be activated in prolog with the flag occurs_check.
You can do this with the following predicate:
            set_prolog_flag(occurs_check, true).
If you test afterwards for unification: A=f(A) you get "false." back.
Without occurs check you get "A = f(A)."

If you don't want to activate it generally you can use the predicate
unify_with_occurs_check/2 to unify with occurs check just a single time.