mylast([X],X).
mylast([_|T],X):- mylast(T,X).

myMax([Max],Max).
myMax([H1,H2|T],Max):- H1>=H2,myMax([H1|T],Max);H2>=H1,myMax([H2|T],Max).

mySum(List,Sum):- mySum(List, 0, Sum).
mySum([], Arith, Sum):- Sum is Arith.
mySum([X|Xs], PartialSum, Sum):- mySum(Xs, X + PartialSum, Sum).

myOrdered([H1,H2|T]):- H1=<H2,myOrderedUp([H2|T]).
myOrdered([H1,H2|T]):- H1>=H2,myOrderedDown([H1|T]).
myOrderedUp([H1,H2|T]):- H1=<H2,myOrderedUp([H2|T]).
myOrderedUp([_]).
myOrderedDown([H1,H2|T]):- H1>=H2,myOrderedDown([H2|T]).
myOrderedDown([_]).

