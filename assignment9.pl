kinder(X,L):- findall(Z, move(X,Z), L).

breitensuche(S,Z,N):-                 kinder(S,K),
                                      bfs(Z, K, [],0,N).
bfs(Z, [Z|_],_,Num,Num):- !.
bfs(Z, [X|RestOpen],Closed,Num,N):-   member(X,Closed),!,
                                      bfs(Z,RestOpen,Closed,Num,N).
bfs(Z,[X|RestOpen],Closed,Num,N):-    kinder(X,L),
                                      NewNum is (Num+1),
                                      append(RestOpen,L,Open),
                                      bfs(Z,Open,[X|Closed],NewNum,N).

tiefensuche(S,Z,N):-                  kinder(S,K),
                                      dfs(Z, K, [],0,N).
dfs(Z, [Z|_],_,Num,Num):- !.
dfs(Z, [X|RestOpen],Closed,Num,N):-   member(X,Closed),!,
                                      dfs(Z,RestOpen,Closed,Num,N).
dfs(Z,[X|RestOpen],Closed,Num,N):-    kinder(X,L),
                                      NewNum is (Num+1),
                                      append(L,RestOpen,Open),
                                      dfs(Z,Open,[X|Closed],NewNum,N).


astern(S,Z,N):- heuristik(S,Z,TS),
                kinder(S,TS,Z,L),
                astar(Z,L,[],0,N).

astar(Z,[[Z,_]|_],_,Num,Num):- !.
astar(Z,[[X,T]|RestOpen],Closed,Num,N):-    member([X,T1],Closed),
                                            T>=T1,!,
                                            astar(Z,RestOpen,Closed,Num,N).
astar(Z,[[X,T]|RestOpen],Closed,Num,N):-    kinder(X,T,Z,L),
                                            NewNum is (Num+1),
                                            append(L,RestOpen,Open),
                                            predsort(compareCosts,Open,NewOpen),
                                            astar(Z,NewOpen,[[X,T]|Closed],NewNum,N).




heuristik(board(X1,X2,X3,X4,X5,X6,X7,X8,X9),
            board(Z1,Z2,Z3,Z4,Z5,Z6,Z7,Z8,Z9),N):-
                    check(X1,Z1,0,Num1),
                    check(X2,Z2,Num1,Num2),
                    check(X3,Z3,Num2,Num3),
                    check(X4,Z4,Num3,Num4),
                    check(X5,Z5,Num4,Num5),
                    check(X6,Z6,Num5,Num6),
                    check(X7,Z7,Num6,Num7),
                    check(X8,Z8,Num7,Num8),
                    check(X9,Z9,Num8,N).

check(b,_,Current,Current):-!.
check(X,Z,Current,Current):- X = Z,!.
check(_,_,Current,New):- New is (Current+1).

% T​(= Anzahl Züge bis Zustand X​ + geschätzte Kosten bis zum Zielzustand Z​)
kinder(X,T,Z,L):- findall(K,kind(X,T,Z,K),L).
kind(X,T,Z,K):- heuristik(X,Z,NX),
                  ZuegeBisX is (T-NX),
                  kinder(X,LK),
                  member(KState,LK),
                  heuristik(KState,Z,NK),
                  TK is (ZuegeBisX+NK),
                  K = [KState,TK].

compareCosts(X,[_,K1],[_,K2]) :- K1 = K2, X = <,!.
compareCosts(X,[_,K1],[_,K2]) :- compare(X, K1, K2).







% teste alle Rotationen durch
% und drehe anschließend wieder in den Urzustand zurück
move(board(X1,X2,X3,X4,X5,X6,X7,X8,X9), Folgezustand):-
          MoeglicheRotationen = [(board(X1,X2,X3,X4,X5,X6,X7,X8,X9),0),
                                (board(X3,X6,X9,X2,X5,X8,X1,X4,X7),1),
                                (board(X9,X8,X7,X6,X5,X4,X3,X2,X1),2),
                                (board(X7,X4,X1,X8,X5,X2,X9,X6,X3),3)],
                    member((Zustand,BackRotations), MoeglicheRotationen),
                    isMove(Zustand, RotierterFolgezustand),
                    rotate(RotierterFolgezustand, Folgezustand, BackRotations).

% Grundlegend gibt es nur diese 3 Ausgangszustände,
% hier sind die Folgezustände dazu definiert
isMove(board(b,X2,X3,X4,X5,X6,X7,X8,X9),Folgezustand):-
        member(Folgezustand,
          [board(X4,X2,X3,b,X5,X6,X7,X8,X9),board(X2,b,X3,X4,X5,X6,X7,X8,X9)]).
isMove(board(X1,b,X3,X4,X5,X6,X7,X8,X9),Folgezustand):-
        member(Folgezustand,
          [board(X1,X5,X3,X4,b,X6,X7,X8,X9),
           board(X1,X3,b,X4,X5,X6,X7,X8,X9),
           board(b,X1,X3,X4,X5,X6,X7,X8,X9)]).
isMove(board(X1,X2,X3,X4,b,X6,X7,X8,X9),board(X1,b,X3,X4,X2,X6,X7,X8,X9)).

% rotiert X-mal nach rechts
rotate(Zustand, Zustand, 0):-!.
rotate(board(X1,X2,X3,X4,X5,X6,X7,X8,X9),XMalRotierterZustand,X):-
          Rotiert = board(X7,X4,X1,X8,X5,X2,X9,X6,X3),
          Y is X-1,
          rotate(Rotiert, XMalRotierterZustand, Y).
