Aufgabe 1:

a. ∀Z ∃Y ∀X (f(X,Y) <=> (f(X,Z) & ~f(X,X)))
	∀Z ∃Y ∀X ((f(X,Y) => (f(X,Z) & ~f(X,X))) & (f(X,Y) <= (f(X,Z) & ~f(X,X))))
	∀Z ∃Y ∀X ((~f(X,Y) | (f(X,Z) & ~f(X,X))) & (f(X,Y) | ~(f(X,Z) & ~f(X,X))))
	∀Z ∃Y ∀X ((~f(X,Y) | (f(X,Z) & ~f(X,X))) & (f(X,Y) | ~f(X,Z) | f(X,X)))
	∃Y ∀X ((~f(X,Y) | (f(X,Z) & ~f(X,X))) & (f(X,Y) | ~f(X,Z) | f(X,X))) y = {Z}
	∀X ((~f(X,y(Z)) | (f(X,Z) & ~f(X,X))) & (f(X,y(Z) | ~f(X,Z) | f(X,X)))
	(~f(X,y(Z)) | (f(X,Z) & ~f(X,X))) & (f(X,y(Z) | ~f(X,Z) | f(X,X))

	(~f(X,y(Z)) | f(X,Z)) & (~f(X,y(Z)) | ~f(X,X)) & (f(X,y(Z) | ~f(X,Z) | f(X,X))

entspricht Musterlösung:
{~f(A,sk1(B)) | f(A,B), ~f(A,sk1(B)) | ~f(A,A), ~f(A,B) | f(A,A) | f(A,sk1(B))}
(X=A, y()=sk1(), Z=B)

b. ∀X ∀Y (q(X,Y) <=> ∀Z (f(Z,X) <=> f(Z,Y)))
	∀X ∀Y (q(X,Y) <=> ∀Z [f(Z,X) => f(Z,Y) & (f(Z,X) <= f(Z,Y))])
	∀X ∀Y [(q(X,Y) => ∀Z [(f(Z,X) => f(Z,Y)) & ((f(Z,X) <= f(Z,Y)))])  &  (q(X,Y) <= ∀Z [(f(Z,X) => f(Z,Y)) & (f(Z,X) <= f(Z,Y))])]
	∀X ∀Y [(~q(X,Y) | ∀Z [(~f(Z,X) | f(Z,Y)) & ((f(Z,X) | ~f(Z,Y)))])  &  (q(X,Y) | ~∀Z [(~f(Z,X) | f(Z,Y)) & (f(Z,X) | ~f(Z,Y)]))]
	∀X ∀Y [(~q(X,Y) | ∀Z [(~f(Z,X) | f(Z,Y)) & ((f(Z,X) | ~f(Z,Y)))])  &  (q(X,Y) | ∃Z [~(~f(Z,X) | f(Z,Y)) | ~(f(Z,X) | ~f(Z,Y))])]
	∀X ∀Y [(~q(X,Y) | ∀Z [(~f(Z,X) | f(Z,Y)) & ((f(Z,X) | ~f(Z,Y)))])  &  (q(X,Y) | ∃Z [(f(Z,X) & ~f(Z,Y)) | (~f(Z,X) & f(Z,Y))])]
	∀X ∀Y [∀Z (~q(X,Y) | [(~f(Z,X) | f(Z,Y)) & ((f(Z,X) | ~f(Z,Y)))])  &  ∃Z (q(X,Y) | [(f(Z,X) & ~f(Z,Y)) | (~f(Z,X) & f(Z,Y))])]
	∀X ∀Y [∀Z (~q(X,Y) | [(~f(Z,X) | f(Z,Y)) & ((f(Z,X) | ~f(Z,Y)))])  &  ∃Z (q(X,Y) | [(f(Z,X) & ~f(Z,Y)) | (~f(Z,X) & f(Z,Y))])]
	∀Z (~q(X,Y) | [(~f(Z,X) | f(Z,Y)) & ((f(Z,X) | ~f(Z,Y)))])  &  ∃Z (q(X,Y) | [(f(Z,X) & ~f(Z,Y)) | (~f(Z,X) & f(Z,Y))]) y={X,Y}
	(~q(X,Y) | [(~f(Z,X) | f(Z,Y)) & ((f(Z,X) | ~f(Z,Y)))])  &  (q(X,Y) | [(f(z(X,Y),X) & ~f(z(X,Y),Y)) | (~f(z(X,Y),X) & f(z(X,Y),Y))])
	(~q(X,Y) | ~f(Z,X) | f(Z,Y)) & (~q(X,Y) | f(Z,X) | ~f(Z,Y))  &  (q(X,Y) | [((f(z(X,Y),X) & ~f(z(X,Y),Y)) | ~f(z(X,Y),X)) & ((f(z(X,Y),X) & ~f(z(X,Y),Y)) | f(z(X,Y),Y))])
	(~q(X,Y) | ~f(Z,X) | f(Z,Y)) & (~q(X,Y) | f(Z,X) | ~f(Z,Y))  &  (q(X,Y) | [(f(z(X,Y),X) | ~f(z(X,Y),X))
			& (~f(z(X,Y),Y) | ~f(z(X,Y),X)) & (f(z(X,Y),X) | f(z(X,Y),Y)) & (~f(z(X,Y),Y) | f(z(X,Y),Y)))])

	(~q(X,Y) | ~f(Z,X) | f(Z,Y)) & (~q(X,Y) | f(Z,X) | ~f(Z,Y))
	 	& (q(X,Y) | f(z(X,Y),X) | ~f(z(X,Y),X)) & (q(X,Y) | ~f(z(X,Y),Y) | ~f(z(X,Y),X)) & (q(X,Y) | f(z(X,Y),X) | f(z(X,Y),Y)) & (q(X,Y) | ~f(z(X,Y),Y) | f(z(X,Y),Y))

entspricht Musterlösung (Vorsicht andere Reihenfolge der Terme):
{ ~q(A,B) | ~ f(C,A) | f(C,B), ~ q(A,B) | ~ f(C,B) | f(C,A), f(sk1(A,B),B) | f(sk1(A,B),A) | q(B,A),
f(sk1(A,B),B) | ~ f(sk1(A,B),B) | q(B,A), ~ f(sk1(A,B),A) | f(sk1(A,B),A) | q(B,A), ~ f(sk1(A,B),A) | ~f(sk1(A,B),B) | q(B,A) }
Lösung nicht optimal gekürzt? 2 Terme fallen weg, weil immer TRUE?? -> f(sk1(A,B),B) | ~ f(sk1(A,B),B) | q(B,A), ~ f(sk1(A,B),A) | f(sk1(A,B),A) | q(B,A)

c. ∀X ∃Y ((p(X,Y) <= ∀X ∃T q(Y,X,T) ) => r(Y))
	∀X ∃Y (~(p(X,Y) | ~∀X ∃T q(Y,X,T) ) | r(Y))
	∀X ∃Y ((~p(X,Y) & ~~∀X ∃T q(Y,X,T) ) | r(Y))
	∀X ∃Y ((~p(X,Y) & ∃T q(Y,X,T) ) | r(Y))
	∀X ∃Y ∃T ((~p(X,Y) & q(Y,X,T) ) | r(Y))
	∃Y ∃T ((~p(X,Y) & q(Y,X,T) ) | r(Y)) y={X}
	∃T ((~p(X,y(X)) & q(y(X),X,T) ) | r(y(X))) y={X}
	((~p(X,y(X)) & q(y(X),X,t(X)) ) | r(y(X)))
	(~p(X,y(X))	| r(y(X))) & (q(y(X),X,t(X))  | r(y(X)))

Folgende Musterlösung ist unsere Wissens nach falsch.
Der Term q(sk1(A),B,sk2(B,A)) kann nicht stimmen.
Es kann nur noch eine Variable geben, die anderen beiden sind skolemisiert
und die Abhängigkeit der dort angegebenen Skolemfunktion ist auch falsch.
Musterlösung: { ~ p(A,sk1(A)) | r(sk1(A)), q(sk1(A),B,sk2(B,A)) | r(sk1(A)) }


d. ∀X ∀Z (p(X,Z) => ∃Y ~ (q(X,Y) | ~r(Y,Z)))
	∀X ∀Z (~p(X,Z) | ∃Y ~ (q(X,Y) | ~r(Y,Z)))
	∀X ∀Z (~p(X,Z) | ∃Y (~q(X,Y) & r(Y,Z)))
	∀X ∀Z ∃Y (~p(X,Z) | (~q(X,Y) & r(Y,Z)))
	∃Y (~p(X,Z) | (~q(X,Y) & r(Y,Z))) y = {X,Z}
	(~p(X,Z) | (~q(X,y(X,Z)) & r(y(X,Z),Z)))
	(~p(X,Z) | ~q(X,y(X,Z))) & (~p(X,Z) | r(y(X,Z),Z))

	Lösung: { ~p(X,Z) | ~q(X,y(X,Z)) , ~p(X,Z) | r(y(X,Z),Z) }


Aufgabe 2:
(Der Übersicht halber wurde (the_kangaroo) weggelassen...)

S = { ~ in_house | cat,
		~ gazer | suitable_pet,
		~ detested | avoided,
		~ carnivore | prowler,
		~ cat | mouse_killer,
		~ takes_to_me | in_house,
		~ kangaroo | ~ suitable_pet,
		~ mouse_killer | carnivore,
		takes_to_me | detested,
		~ prowler | gazer,
		kangaroo,
		~ avoided }

DPLL(S)
Unit clause: kangaroo, S = Simplify(S,kangaroo)

S = { ~ in_house | cat,
		~ gazer | suitable_pet,
		~ detested | avoided,
		~ carnivore | prowler,
		~ cat | mouse_killer,
		~ takes_to_me | in_house,
		~ suitable_pet,
		~ mouse_killer | carnivore,
		takes_to_me | detested,
		~ prowler | gazer,
		~ avoided }

Unit clause: ~ suitable_pet, S = Simplify(S,~suitable_pet)

S = { ~ in_house | cat,
		~ gazer,
		~ detested | avoided,
		~ carnivore | prowler,
		~ cat | mouse_killer,
		~ takes_to_me | in_house,
		~ mouse_killer | carnivore,
		takes_to_me | detested,
		~ prowler | gazer,
		~ avoided }

Unit clause: ~ gazer, S = Simplify(S,~gazer)

S = { ~ in_house | cat,
		~ detested | avoided,
		~ carnivore | prowler,
		~ cat | mouse_killer,
		~ takes_to_me | in_house,
		~ mouse_killer | carnivore,
		takes_to_me | detested,
		~ prowler,
		~ avoided }

Unit clause: ~prowler, S = Simplify(S,~prowler)

S = { ~ in_house | cat,
		~ detested | avoided,
		~ carnivore,
		~ cat | mouse_killer,
		~ takes_to_me | in_house,
		~ mouse_killer | carnivore,
		takes_to_me | detested,
		~ avoided }

Unit clause: ~carnivore, S = Simplify(S,~carnivore)

S = { ~ in_house | cat,
		~ detested | avoided,
		~ cat | mouse_killer,
		~ takes_to_me | in_house,
		~ mouse_killer,
		takes_to_me | detested,
		~ avoided }

Unit clause: ~mouse_killer, S = Simplify(S,~mouse_killer)

S = { ~ in_house | cat,
		~ detested | avoided,
		~ cat,
		~ takes_to_me | in_house,
		takes_to_me | detested,
		~ avoided }

Unit clause: ~cat, S = Simplify(S,~cat)

S = { ~ in_house,
		~ detested | avoided,
		~ takes_to_me | in_house,
		takes_to_me | detested,
		~ avoided }

Unit clause: ~in_house, S = Simplify(S,~in_house)

S = { ~ detested | avoided,
		~ takes_to_me,
		takes_to_me | detested,
		~ avoided }

Unit clause ~takes_to_me, S = Simplify(S,~takes_to_me)

S = { ~ detested | avoided,
		detested,
		~ avoided }

Unit clause: ~avoided, S = Simplify(S,~avoided)

S = { ~ detested,
		detested }

Because {detested} and {~detested} are in S => return "unsatisfiable"
=> Conjecture is true!

Aufgabe 3:

V = {X,Y}
F = {vater_von/1, mutter_von/1, max/0}
P = {verheiratet/2}

D = { 	max,
		vater_von(max),
		mutter_von(max),
		vater_von(mutter_von(max)),
		mutter_von(vater_von(max)),
		vater_von(vater_von(max)),
		mutter_von(mutter_von(max)),
		verheiratet(vater_von(max),mutter_von(max)),
		verheiratet(vater_von(vater_von(max)),mutter_von(max)),
		verheiratet(vater_von(max),mutter_von(mutter_von(max))),
		... }


R = { 	verheiratet(vater_von(max),mutter_von(max)) -> TRUE,
		verheiratet(max, max) -> FALSE,
		verheiratet(max, vater_von(max)) -> FALSE,
		verheiratet(mutter_von(max),max) -> FALSE,
		verheiratet(vater_von(max), vater_von(vater_von(max))) -> FALSE,
		verheiratet(vater_von(max), vater_von(vater_von(vater_von(max)))) -> FALSE,
		verheiratet(mutter_von(mutter_von(max)), mutter_von(max)) -> FALSE,
		verheiratet(vater_von(max),vater_von(max)) -> FALSE,
		verheiratet(mutter_von(max),mutter_von(max)) -> FALSE,
		verheiratet(mutter_von(mutter_von(mutter_von(max))),mutter_von(max)) -> FALSE }
