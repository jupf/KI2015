male(gen1m1).
male(gen1m2).
male(gen1m3).
male(gen1m4).
male(gen2m1).
male(gen2m2).
male(gen2m3).
male(gen3m1).
male(gen4m1).


female(gen1f1).
female(gen1f2).
female(gen1f3).
female(gen1f4).
female(gen2f1).
female(gen2f2).
female(gen2f3).
female(gen3f1).
female(gen4f1).

father(gen1m1,gen2m1).
father(gen1m2,gen2m2).
father(gen1m2,gen2f3).
father(gen1m3,gen2f1).
father(gen1m4,gen2f2).
father(gen1m4,gen2m3).
father(gen2m2,gen3m1).
father(gen2m3,gen3f1).
father(gen3m1,gen4m1).
father(gen3m1,gen4f1).

mother(gen1f1,gen2m1).
mother(gen1f2,gen2m2).
mother(gen1f2,gen2f3).
mother(gen1f3,gen2f1).
mother(gen1f4,gen2f2).
mother(gen1f4,gen2m3).
mother(gen2f2,gen3m1).
mother(gen2f3,gen3f1).
mother(gen3f1,gen4m1).
mother(gen3f1,gen4f1).

parent(X,Y):- father(X,Y);mother(X,Y).
son(X,Y):- male(X),parent(Y,X).
daughter(X,Y):- female(X),parent(Y,X).
child(X,Y):- son(X,Y);daughter(X,Y).
%brother(X,Y):- parent(Z,X),parent(Z,Y),male(X),X\=Y.
brother(X,Y):- son(X,Z),child(Y,Z),X\=Y.
sister(X,Y):- daughter(X,Z),child(Y,Z),X\=Y.
%sister(X,Y):- parent(Z,X),parent(Z,Y),female(X),X\=Y.
%grandfather(X,Y):- child(Z,X),child(Y,Z),male(X).
grandfather(X,Y):- father(X,Z),parent(Z,Y).
grandmother(X,Y):- mother(X,Z),parent(Z,Y).
successor(X,Y):- child(X,Y);child(Z,Y),successor(X,Z).
predecessor(X,Y):- successor(Y,X).
