depth(Goals, [X|_], Closed):- member(X,Goals),!,write(Closed).
depth(Goals, [X|RestOpen], Closed):-  member(X,Closed),!,
                                      depth(Goals, RestOpen, Closed).
depth(Goals, [X|RestOpen], Closed):-  childs(X, L),
                                      append(L, RestOpen, Open),
                                      depth(Goals, Open, [X|Closed]).

%childs(X, L):- setof(Y, con(X,Y), L),!.
%childs(_,[]).

childs(X, L):- findall(Y, con(X,Y), L).


con(a,b).
con(b,c).
con(c,d).
con(c,z).
con(d,e).
con(b,d).
con(a,f).
