# Assignment 10 - Huning - Pfeifer
### Exercise 1
```prolog
% zerlegt erst alles in Summanden wegen Op.-Priorität
expr(add(Y,Z)) --> term(Y), "+", expr(Z).
expr(X)        --> term(X).
term(mul(Y,Z)) --> atom(Y), "*", term(Z).
term(X)        --> atom(X).
atom(X)        --> n(X).
atom(X)        --> "(", expr(X), ")".

% ---- AB HIER UNVERÄNDERT ----
n(X)        --> z(X,_).
n(X)        --> z(Y,_), ".", z(Z,A), {X is Y+10^(-A)*Z}.
n(X)        --> "-", z(Y,_), {X is -Y}.
n(X)        --> "-", z(Y,_), ".", z(Z,A), {X is -(Y+10^(-A)*Z)}.
% z(wert,anzahl der ziffern)
z(X,B)      --> num(Y), z(Z,A), {X is Y*10^A+Z, B is A+1}.
z(X,1)      --> num(X).
num(1)      --> "1".
num(2)      --> "2".
num(3)      --> "3".
num(4)      --> "4".
num(5)      --> "5".
num(6)      --> "6".
num(7)      --> "7".
num(8)      --> "8".
num(9)      --> "9".
num(0)      --> "0".
```


### Exercise 2
```prolog
% algebra1 ist die Regel für CF1 ==> CF2
% Also das combine aus der Vorlesung

% Wenn sich beide zu null addieren, dann kommt definitiv null raus
algebra1(CF1,CF2,AdjustedCF):-
	X is CF1+CF2,
	X =:= 0,
	AdjustedCF = 0,!.

% Wenn CF1 gleich null ist, dann ist CF2 das Ergebnis
algebra1(CF1,CF2,AdjustedCF):-
	CF1 =:= 0,
	AdjustedCF = CF2,!.

% Wenn CF2 gleich null ist, dann ist CF1 das Ergebnis
algebra1(CF1,CF2,AdjustedCF):-
	CF2 =:= 0,
	AdjustedCF = CF1,!.

% Addiere und skaliere zwei Werte größer null
algebra1(CF1,CF2,AdjustedCF):-
	CF1 >= 0,
	CF2 >= 0,
	X is CF1 + CF2 * (100-CF1)/100,
	AdjustedCF is round(X),!.

% Addiere und skaliere zwei Werte kleiner null
algebra1(CF1,CF2,AdjustedCF):-
	CF1 < 0,
	CF2 < 0,
	X is -(-CF1-CF2*(100+CF1))/100,
	AdjustedCF is round(X),!.

% Kombiniere zwei einen Wert größer null und einen Wert kleiner null
algebra1(CF1,CF2,AdjustedCF):-
	(CF1 < 0; CF2 < 0),
	(CF1 > 0; CF2 > 0),
	abs(CF1,CF1abs), abs(CF2,CF2abs),
	AbsMinCF is min(CF1abs,CF2abs),
	X is 100*(CF1+CF2)/(100-AbsMinCF),
	AdjustedCF is round(X).

% Dies ist die Funktion, wenn zwei Konfidenzen verundet werden
algebra2(CF1,CF2,NewCF):- NewCF is min(CF1,CF2).

% Dies ist die Funktion, wenn zwei Konfidenzen verodert werden
algebra3(CF1,CF2,NewCF):- NewCF is max(CF1,CF2).
```
