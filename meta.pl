solve(true):-   !.
solve((A;B)):-
        solve(A);
        solve(B),!.
solve((A,B)):- !,
        solve(A),
        solve(B).

solve(A):-
        system(A),!,
        call(A).
solve(A):-
        clause(A,B),
        solve(B).
system(=(_,_)).
system(==(_,_)).
system(fail).
system(nl).
system(read(_)).
system(write(_)).
system(is(_,_)).
system(>(_,_)).
system(<(_,_)).
system(clause(_,_)).
system(call(_)).
system(var(_)).

vater(adam,abel).
vater(adam,cain):-!.
vater(abel,isaac).
opa(X,Y):-!,vater(X,Z),vater(Z,Y).
a:-c.
a:- !,fail.
a:- b,!,c.
a:-c.
b.
c.
