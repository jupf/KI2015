## KI2015 Assignment 6

### Aufgabe 1



### Aufgabe 2

### 2a: ◇(P => ◻P) in T

```plaintext

(1) ~(◇(P => ◻P))                       (1)
(1) ~(P => ◻P)    durch ~◇T             (2)
(1)     P         durch ~=>             (3)
(1)   ~◻P         durch ~=>             (4)
(1.1)  ~P         durch ~◻              (5)
(1.1) ~(P => ◻P)  durch ~◇ aus (1)      (6)
(1.1)   P         durch ~=>             (7)
(1.1) ~◻P         durch ~=>             (8)

      ==> Widerspruch durch (7) und (5)

```
Damit ist die Behauptung:  ◇(P => ◻P) in T  bewiesen.

### 2b: (◻P & ◻Q) => ◻(◻P & ◻Q) in K4

```plaintext

(1) ~((◻P & ◻Q) => ◻(◻P & ◻Q))                                  (1)
(1)   (◻P & ◻Q)                     durch ~=>                   (2)
(1)   ~◻(◻P & ◻Q)                   durch ~=>                   (3)
(1)   ◻P                            durch &                     (4)
(1)   ◻Q                            durch &                     (5)
(1.1) ~(◻P & ◻Q)                    durch ~◻                    (6)
(1.1) ~◻P        |       (1.1) ~◻Q  durch ~&                    (7)
(1.1)  ◻P        |       (1.1)  ◻Q  durch ◻4 aus (4) und (5)    (8)

      ==> Widerspruch durch (7) und (8) in beiden Zweigen

```
Damit ist die Behauptung:  (◻P & ◻Q) => ◻(◻P & ◻Q) in K4  bewiesen.

### 2c: ◻(P => Q) ∨ ◻¬◻(¬Q => ¬P) in S5

```plaintext

(1)     ~(◻(P => Q) | ◻~◻(~Q => ~P))                            (1)
(1)     ~◻(P => Q)                  durch ~|                    (2)
(1)     ~◻~◻(~Q => ~P)              durch ~|                    (3)
(1.1)   ~(P => Q)                   durch ~◻ aus (2)            (4)
(1.1)     P                         durch ~=>                   (5)
(1.1)    ~Q                         durch ~=>                   (6)
(1.2)   ~~◻(~Q => ~P)               durch ~◻ aus (3)            (7)
(1.2)     ◻(~Q => ~P)               durch ~~                    (8)
(1.1)     ◻(~Q => ~P)               durch 4r                    (9)
(1.1)     (~Q => ~P)                durch T                     (10)
(1.1)    Q         |      (1.1) ~P  durch =>                    (11)

    ==> Widerspruch in (11) und (5) und Widerspruch in (11) und (6)

```
Damit ist die Behauptung:  ◻(P => Q) ∨ ◻¬◻(¬Q => ¬P) in S5  bewiesen.
