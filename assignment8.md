# Assignment 8 - Huning - Pfeifer
### Exercise 1
```prolog
solve(true):-   !.
solve((A;B)):-  
        solve(A);
        solve(B),!.
solve((A,B)):-
        solve(A),
        solve(B),!.
solve(A):-
        system(A),!,
        call(A).
solve(A):-
        clause(A,B),
        solve(B).
system(=(_,_)).
system(==(_,_)).
system(fail).
system(nl).
system(read(_)).
system(write(_)).
system(is(_,_)).
system(>(_,_)).
system(<(_,_)).
system(clause(_,_)).
system(call(_)).
system(var(_)).
```

### Exercise 2a
Man muss lediglich die Regeln für prove ändern:  

```prolog
prove(true,_) :- !.
prove((Goal;Rest),Hist) :- !,
	(prove(Goal,Hist);
	prove(Rest,Hist)).
prove((Goal,Rest),Hist) :- !,
	prove(Goal,Hist),
	prove(Rest,Hist).
prove(Goal,Hist) :-
	prov(Goal,[Goal|Hist]).
```
Und zwar vor die Regel: prove((Goal,Rest),Hist):-....  


### Exercise 2b
```prolog
% electronics.nkb - an electronics identification system for use with the
% Native shell.


% top_goal where Native starts the inference.
top_goal(X) :- electronics(X).

electronics(desktop_pc):-
  input_device_type(external),
  screen_type(external),
  size(large).

electronics(laptop):-
  input_device_type(mobile),
  screen_type(integrated),
  size(medium).

electronics(smartphone):-
  input_device_type(mobile),
  screen_type(integrated),
  size(handheld).


input_device_type(external):-
  input_device(keyboard);
  input_device(mouse).

input_device_type(mobile):-
  input_device(touch);
  input_device(bluetooth_device).

family(X):- ask(family,X).
input_device(X):- ask(input_device,X).
screen_type(X):- ask(screen_type,X).
size(X):- menuask(size,X,[large,medium,handheld]).

% lässt verschiedene Belegungen für ein Attribut zu
multivalued(input_device).
```
