% Exercise 1
% 1.a
% G = ({a; b; c; d; e};{(a; b);(a; c);(b; d);(b; e);(c; d);(c; e);(d; e)})
% representation through the edges:
con(a,b).
con(b,a).
con(a,c).
con(c,a).
con(b,d).
con(d,b).
con(b,e).
con(e,b).
con(c,d).
con(d,c).
con(c,e).
con(e,c).
con(d,e).
con(e,d).

% 1.b
path(X,Y,P):- path(X,Y,[X],P).
path(X,Y,Path,P):-  con(X,Y),
                    append(Path, [Y], NewPath),
                    NewPath = P.
path(X,Y,Path, P):- con(X,Z),
                    not(member(Z, Path)),
                    not(Z=Y),
                    append(Path,[Z],NewPath),
                    path(Z,Y,NewPath,P).

% 1.c
allPaths(X,Y,L):-findall(P, path(X,Y,P), L).

% 1.d
maxPath(X,Y,M):- allPaths(X,Y,L),
                 longestList(L,M).


longestList([List1,List2|LS], Longest):-
            length(List1, Length1),
            length(List2, Length2),
            (Length1 >= Length2 ->  longestList([List1|LS], Length1, Longest);
                                    longestList([List2|LS], Length2, Longest)).

longestList([Longest], _, Longest):-!.
longestList([List1,List2|LS], Length1, Longest):-
            length(List2, Length2),
            (Length1 >= Length2 -> longestList([List1|LS], Length1, Longest);
                                   longestList([List2|LS], Length2, Longest)).

% Exercise 2

% teilt eine Liste in zwei Partitionen,
% eine mit Elementen größer gleich X und eine mit Elementen kleiner als X
myPartition([H|T],X,Smaller,Bigger):-
              (H >= X ->  myPartition(T,X,[],[H],Smaller,Bigger);
                          myPartition(T,X,[H],[],Smaller,Bigger)).
myPartition([],_,Smaller,Bigger,Smaller,Bigger).
myPartition([H|T],X,Smaller,Bigger,ResultSmaller, ResultBigger):-
    (H >= X ->  myPartition(T,X,Smaller,[H|Bigger],ResultSmaller,ResultBigger);
                myPartition(T,X,[H|Smaller],Bigger,ResultSmaller,ResultBigger)).

qSort([], []):-!.
qSort([X], [X]):-!.
qSort(List,SortedList):-  length(List,Length),
                          random_between(1,Length,PivotIndex),
                          nth1(PivotIndex,List,Pivot,Rest),
                          myPartition(Rest,Pivot,Smaller,Bigger),
                          qSort(Smaller, SortedSmaller),
                          qSort(Bigger, SortedBigger),
                          append(SortedSmaller, [Pivot|SortedBigger], Sorted),
                          SortedList = Sorted.

% Exercise 3

% teste alle Rotationen durch
% und drehe anschließend wieder in den Urzustand zurück
move(board(X1,X2,X3,X4,X5,X6,X7,X8,X9), Folgezustand):-
          MoeglicheRotationen = [(board(X1,X2,X3,X4,X5,X6,X7,X8,X9),0),
                                (board(X3,X6,X9,X2,X5,X8,X1,X4,X7),1),
                                (board(X9,X8,X7,X6,X5,X4,X3,X2,X1),2),
                                (board(X7,X4,X1,X8,X5,X2,X9,X6,X3),3)],
                    member((Zustand,BackRotations), MoeglicheRotationen),
                    isMove(Zustand, RotierterFolgezustand),
                    rotate(RotierterFolgezustand, Folgezustand, BackRotations).

% Grundlegend gibt es nur diese 3 Ausgangszustände,
% hier sind die Folgezustände dazu definiert
isMove(board(b,X2,X3,X4,X5,X6,X7,X8,X9),Folgezustand):-
        member(Folgezustand,
          [board(X4,X2,X3,b,X5,X6,X7,X8,X9),board(X2,b,X3,X4,X5,X6,X7,X8,X9)]).
isMove(board(X1,b,X3,X4,X5,X6,X7,X8,X9),Folgezustand):-
        member(Folgezustand,
          [board(X1,X5,X3,X4,b,X6,X7,X8,X9),
           board(X1,X3,b,X4,X5,X6,X7,X8,X9),
           board(b,X1,X3,X4,X5,X6,X7,X8,X9)]).
isMove(board(X1,X2,X3,X4,b,X6,X7,X8,X9),board(X1,b,X3,X4,X2,X6,X7,X8,X9)).

% rotiert X-mal nach rechts
rotate(Zustand, Zustand, 0):-!.
rotate(board(X1,X2,X3,X4,X5,X6,X7,X8,X9),XMalRotierterZustand,X):-
          Rotiert = board(X7,X4,X1,X8,X5,X2,X9,X6,X3),
          Y is X-1,
          rotate(Rotiert, XMalRotierterZustand, Y).
